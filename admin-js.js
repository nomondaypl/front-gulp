'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify-es').default;
var notify = require('gulp-notify');

var strip = require('gulp-strip-comments');
var stripDebug = require('gulp-strip-debug');

function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

gulp.task('admin-js', function () {
    gulp.src(["vendor/nomondaypl/nm-admin/assets/dev/js/**/*.js"])
        .pipe(concat('admin.js'))
        .pipe(strip())
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest("vendor/nomondaypl/nm-admin/assets/dist/js"))
        .pipe(notify({
            title: 'Gulp',
            message: "Wygenerowany Plik: <%= file.relative %> @ <%= options.date %>",
            templateOptions: {
                date: formatDate(new Date())
            }
        }));
});
