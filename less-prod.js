'use strict';

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const notify = require('gulp-notify');
const cssmin = require('gulp-cssmin');
const less = require('gulp-less');
const path = require('path');
const pjson = require('../../../package.json');
var csspath;

function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

if (pjson.gulpPaths.lessDest) {
    csspath = pjson.gulpPaths.lessDest;
} else {
    csspath = "public/layout/default/dist/css";
}

if (pjson.gulpPaths.less) {
    gulp.task('less-prod', function () {
        return gulp.src(pjson.gulpPaths.less)
            .pipe(sourcemaps.init())
            .pipe(concat('style.css'))
            .pipe(less({
                paths: [path.join(__dirname, 'less', 'includes')]
            }))
            .pipe(cssmin())
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(csspath))
            .pipe(notify({
                title: 'Gulp',
                message: "Wygenerowany Plik: <%= file.relative %> @ <%= options.date %>",
                templateOptions: {
                    date: formatDate(new Date())
                }
            }));
    });
}
