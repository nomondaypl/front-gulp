'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var sass = require('gulp-sass');
var pjson = require('../../../package.json');
var csspath;

function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

if (pjson.gulpPaths.scssDest) {
    csspath = pjson.gulpPaths.scssDest;
} else {
    csspath = "public/layout/default/dist/css";
}

if (pjson.gulpPaths.scss) {
    gulp.task('sass-prod', function () {
        return gulp.src(pjson.gulpPaths.scss)
            .pipe(sourcemaps.init())
            .pipe(concat('style.css'))
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(csspath))
            .pipe(notify({
                title: 'Gulp',
                message: "Wygenerowany Plik: <%= file.relative %> @ <%= options.date %>",
                templateOptions: {
                    date: formatDate(new Date())
                }
            }));
    });
}
