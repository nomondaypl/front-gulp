'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var sass = require('gulp-sass');

function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

gulp.task('admin-sass', function () {
    return gulp.src(["vendor/nomondaypl/nm-admin/assets/dev/scss/style.scss"])
        .pipe(sourcemaps.init())
        .pipe(concat('admin.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("vendor/nomondaypl/nm-admin/assets/dist/css"))
        .pipe(notify({
            title: 'Gulp',
            message: "Wygenerowany Plik: <%= file.relative %> @ <%= options.date %>",
            templateOptions: {
                date: formatDate(new Date())
            }
        }));
});
