'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify-es').default;
var notify = require('gulp-notify');

var strip = require('gulp-strip-comments');
var stripDebug = require('gulp-strip-debug');

var pump = require('pump');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var pjson = require('../../../package.json');


gulp.task('browsersync',['sass', 'js'], function () {
    browserSync.init({
        proxy: "localhost:3000",
        ghostMode: false
    });
    gulp.watch('./public/layout/default/dev/scss/**/*.scss', ['sass']);
    gulp.watch('./public/layout/default/dev/js/**/*.js', ['js']);

    gulp.watch('./public/layout/default/dist/js/**/*.js').on("change", reload);
    gulp.watch('./public/layout/default/dist/css/**/*.css').on("change", reload);
    gulp.watch('./views/default/**/*.hbs').on("change", reload);
});