'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const notify = require('gulp-notify');
const autoprefixer = require('gulp-autoprefixer');
const pjson = require('../../../package.json');


function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

gulp.task('autoprefix', function () {
    gulp.src(pjson.gulpPaths.scssDest + '/**/*.css')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(pjson.gulpPaths.scssDest))
        .pipe(notify({
            title: 'Gulp',
            message: "Wygenerowany Plik: <%= file.relative %> @ <%= options.date %>",
            templateOptions: {
                date: formatDate(new Date())
            }
        }));
});
