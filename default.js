'use strict';

var gulp = require('gulp');
var path = require('path');
var pjson = require('../../../package.json');
var defaultTasks = [];

if (pjson.gulpPaths.js) {
    var jsPath = pjson.gulpPaths.js[Object.keys(pjson.gulpPaths.js).length - 1];
    defaultTasks.push("js");
}

if (pjson.gulpPaths.scss) {
    var scssPath = pjson.gulpPaths.scss[Object.keys(pjson.gulpPaths.scss).length - 1];
    defaultTasks.push("sass");
}

if (pjson.gulpPaths.less) {
    var lessPath = pjson.gulpPaths.less[Object.keys(pjson.gulpPaths.less).length - 1];
    defaultTasks.push("less");
}

gulp.task('default', defaultTasks, function () {
    if (scssPath) {
        gulp.watch(path.dirname(scssPath) + '/**/*.scss', ['sass']);
    }
    if (jsPath) {
        gulp.watch(path.dirname(jsPath) + '/**/*.js', ['js']);
    }
    if (lessPath) {
        gulp.watch(path.dirname(lessPath) + '/**/*.less', ['less']);
    }
});
