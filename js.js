'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify-es').default;
var notify = require('gulp-notify');

var strip = require('gulp-strip-comments');
var stripDebug = require('gulp-strip-debug');

var pump = require('pump');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var pjson = require('../../../package.json');
var jspath;


function formatDate(date) {
    var monthNames = [
        "Stycznia", "Lutego", "Marca",
        "Kwietnia", "Maja", "Czerwca", "Lipca",
        "Sierpnia", "Września", "Października",
        "Listopada", "Grudnia"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' - ' + hour + ':' + min;
}

if (pjson.gulpPaths.jsDest) {
    jspath = pjson.gulpPaths.jsDest;
} else {
    jspath = "public/layout/default/dist/js";
}

gulp.task('js', function () {
    gulp.src(pjson.gulpPaths.js)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(jspath))
        .pipe(notify({
            title: 'Gulp',
            message: "Wygenerowany Plik: <%= file.path %> @ <%= options.date %>",
            templateOptions: {
                date: formatDate(new Date())
            }
        }));
});
